import React, { Fragment } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import GlobalStyle from './styles/GlobalStyles'

import Home from './pages/Home'
import User from './pages/User'

const App = () => {
  return (
    <Fragment>
      <GlobalStyle />
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/users' component={User} />
        </Switch>
      </Router>
    </Fragment>
  )
}

export default App
