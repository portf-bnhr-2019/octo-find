import React from 'react'

import * as UserStyle from '../components/users/StyledUser'

import Brad from '../images/brad.jpg'

const User = () => {
  return (
    <UserStyle.Container>
      <UserStyle.BackButton to='/'>Back to search</UserStyle.BackButton>
      <UserStyle.Wrapper>
        <UserStyle.ProfileCard>
          <UserStyle.Centered>
            <UserStyle.ProfileImg>
              <img src={Brad} alt='User Profile' />
            </UserStyle.ProfileImg>
            <UserStyle.Name>
              Brad Traversy -{' '}
              <UserStyle.Username>
                <a href='https://github.com/'>@bradtr</a>
              </UserStyle.Username>
            </UserStyle.Name>
            <UserStyle.Contact capital>
              <span>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='16'
                  height='16'
                  viewBox='0 0 24 24'
                  fill='none'
                  stroke='currentColor'
                  strokeWidth='2'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  className='feather feather-map-pin'>
                  <path d='M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z'></path>
                  <circle cx='12' cy='10' r='3'></circle>
                </svg>
              </span>
              <span>toronto</span>
            </UserStyle.Contact>
            <UserStyle.CenteredBetween>
              <UserStyle.Contact>
                <span>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='24'
                    height='24'
                    viewBox='0 0 24 24'
                    fill='none'
                    stroke='currentColor'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    className='feather feather-globe'>
                    <circle cx='12' cy='12' r='10'></circle>
                    <line x1='2' y1='12' x2='22' y2='12'></line>
                    <path d='M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z'></path>
                  </svg>
                </span>
                <span>https://traversymedia.com</span>
              </UserStyle.Contact>
              <UserStyle.Contact>
                <span>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='24'
                    height='24'
                    viewBox='0 0 24 24'
                    fill='none'
                    stroke='currentColor'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    className='feather feather-mail'>
                    <path d='M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z'></path>
                    <polyline points='22,6 12,13 2,6'></polyline>
                  </svg>
                </span>
                <span>brad@email.com</span>
              </UserStyle.Contact>
            </UserStyle.CenteredBetween>
          </UserStyle.Centered>
          <UserStyle.Line />
          <UserStyle.Bio>
            <p>
              I am a full stack web developer specializing mostly in
              JavaScript/Node.js but also work with PHP, Python & Rails. I am an
              online instructor for Traversy Media
            </p>
          </UserStyle.Bio>
          <UserStyle.Line />
          <UserStyle.Populer>
            <UserStyle.SeparateReverse>
              <p>18710</p>
              <p>followers</p>
            </UserStyle.SeparateReverse>
            <UserStyle.SeparateReverse>
              <p>21</p>
              <p>following</p>
            </UserStyle.SeparateReverse>
            <UserStyle.SeparateReverse>
              <p>156</p>
              <p>public repos</p>
            </UserStyle.SeparateReverse>
            <UserStyle.SeparateReverse>
              <p>21</p>
              <p>public gists</p>
            </UserStyle.SeparateReverse>
          </UserStyle.Populer>
          <UserStyle.Centered>
            <UserStyle.RoundedButton href='https://github.com/'>
              Hire me
            </UserStyle.RoundedButton>
          </UserStyle.Centered>
        </UserStyle.ProfileCard>
        <UserStyle.ReposAll>
          <h4>Recent repos :</h4>
          <UserStyle.ReposContainer>
            <UserStyle.ReposItem>
              <h3>title</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing.</p>
              <p>asdasd</p>
            </UserStyle.ReposItem>
            <UserStyle.ReposItem>
              <h3>title</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing.</p>
              <p>asdasd</p>
            </UserStyle.ReposItem>
            <UserStyle.ReposItem>
              <h3>title</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing.</p>
              <p>asdasd</p>
            </UserStyle.ReposItem>
            <UserStyle.ReposItem>
              <h3>title</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing.</p>
              <p>asdasd</p>
            </UserStyle.ReposItem>
            <UserStyle.ReposItem>
              <h3>title</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing.</p>
              <p>asdasd</p>
            </UserStyle.ReposItem>
          </UserStyle.ReposContainer>
        </UserStyle.ReposAll>
      </UserStyle.Wrapper>
    </UserStyle.Container>
  )
}

export default User
