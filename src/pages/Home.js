import React from 'react'

import styled, { keyframes } from 'styled-components'

import { device } from '../styles/Devices'

const All = styled.section`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: linear-gradient(
    to right top,
    #4274d3,
    #2c81d3,
    #258ccf,
    #3596c9,
    #4c9ec2
  );

  form {
    margin-bottom: 12vh;
  }
`

const fadein = keyframes`
  from {top: 0; opacity: 0;} 
  to {top: 30px; opacity: 1;}
`

const fadeout = keyframes`
  from {top: 30px; opacity: 1;}
  to {top: 0; opacity: 0;}
`

const Toast = styled.div`
  background-color: #fff;
  color: #3596c9;
  font-size: 2rem;
  padding: 3rem;
  position: absolute;
  top: 1.2rem;
  left: 1.2rem;
  border-radius: 5px;
  text-align: center;
  visibility: hidden;
  z-index: 10;
`

const Container = styled.section`
  padding-right: 1.5rem;
  padding-left: 1.5rem;
  margin-left: auto;
  margin-right: auto;
  width: 100%;

  @media ${device.laptopL} {
    max-width: 100rem;
  }
  @media ${device.laptop} {
    max-width: 96rem;
  }
  @media ${device.tablet} {
    max-width: 72rem;
  }
  @media ${device.mobileL} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileM} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileS} {
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  }
`

const Heading = styled.h1`
  text-align: center;
  color: white;
  font-size: calc(4rem + (68 - 40) * ((100vw - 30rem) / (1600 - 300)));
`

const SearchContainer = styled.div`
  display: flex;
  position: relative;
  border: 1px solid transparent;
  border-radius: 7.5rem;
  display: flex;
  background-color: white;
  margin-bottom: 2rem;
  padding: 0.7rem 2rem;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
`

const Icon = styled.div`
  display: flex;
  align-items: center;
  padding-right: 1.2rem;

  svg {
    color: gray;
  }

  @media ${device.tablet} {
    svg {
      width: 25px;
      height: 25px;
    }
  }
  @media ${device.mobileL} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
  @media ${device.mobileM} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
  @media ${device.mobileS} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
`

const SearchStyle = styled.input`
  position: relative;
  border: 1px solid transparent;
  font-size: calc(1.8rem + (40 - 18) * ((100vw - 30rem) / (1600 - 300)));
  color: #4579b9;
  border-radius: 6px;
  text-align: center;
  text-transform: lowercase;
  display: block;
  margin: calc(1rem + (20 - 10) * ((100vw - 30rem) / (1600 - 300))) 0;
  width: 100%;
  min-width: 50rem;
  background-color: transparent;

  &:focus {
    outline: 0;
  }

  @media ${device.tablet} {
    min-width: 0;
  }
  @media ${device.mobileL} {
    min-width: 0;
  }
  @media ${device.mobileM} {
    min-width: 0;
  }
  @media ${device.mobileS} {
    min-width: 0;
  }
`

const Home = () => {
  return (
    <All>
      <Container>
        <form>
          <Toast>
            {' '}
            <span role='img' aria-label='confused'>
              😫
            </span>{' '}
            please fill the search box
          </Toast>
          <Heading>octofind</Heading>
          <SearchContainer>
            <Icon>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='40'
                height='40'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'
                className='feather feather-search'>
                <circle cx='11' cy='11' r='8'></circle>
                <line x1='21' y1='21' x2='16.65' y2='16.65'></line>
              </svg>
            </Icon>
            <SearchStyle
              type='search'
              name='search'
              placeholder='search user'
              autoFocus
            />
          </SearchContainer>
        </form>
      </Container>
    </All>
  )
}

export default Home
